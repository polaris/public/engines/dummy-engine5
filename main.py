import sys

from requests import HTTPError

from utils import get_statements, save_results


def main(analytics_token):
    try:
        # Retrieve available statements
        statemens = get_statements(analytics_token)

        verbs_per_email = {}
        for statement in statemens:
            email = statement["actor"]["mbox"].split("mailto:")[1]
            verb_name = statement["verb"]["display"]["en-US"]

            if verbs_per_email.get(email) is None:
                verbs_per_email[email] = {verb_name: 1}
            else:
                user_dict = verbs_per_email[email]
                verbs_per_email[email][verb_name] = user_dict.get(verb_name, 0) + 1

        # Built results array
        for email in verbs_per_email:
            sum_user_statements = sum(verbs_per_email[email].values())
            result = [
                {
                    "column1": verb,
                    "column2": int(
                        verbs_per_email[email][verb] / sum_user_statements * 100
                    ),
                }
                for verb in verbs_per_email[email]
            ]

            # Send result to rights engine
            save_results(analytics_token, {"result": result, "context_id": email})
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1])
