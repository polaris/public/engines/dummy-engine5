import requests

API_URL = "http://127.0.0.1:8003"
PAGE_SIZE = 100


def get_statements_page(analytics_token, last_object_id=None):
    payload = (
        {"page_size": PAGE_SIZE, "last_object_id": last_object_id}
        if last_object_id
        else {"page_size": PAGE_SIZE}
    )
    response = requests.post(
        f"{API_URL}/api/v1/provider/data",
        headers={"Authorization": f"Basic {analytics_token}"},
        json=payload,
    )
    response.raise_for_status()

    payload = response.json()
    return payload.get("statements", [])


def get_statements(analytics_token):
    statements = []
    read_all_pages = False
    last_object_id = None
    while not read_all_pages:
        page_statements = get_statements_page(analytics_token, last_object_id)
        statements.extend(page_statements)
        if len(page_statements) == 0:
            read_all_pages = True
        else:
            last_object_id = statements[-1]["_id"]

    return statements


def get_existing_results(analytics_token):
    response = requests.get(
        f"{API_URL}/api/v1/provider/results",
        headers={"Authorization": f"Basic {analytics_token}"},
    )
    response.raise_for_status()
    data = response.json()
    return data


def save_results(analytics_token, results):
    response = requests.post(
        f"{API_URL}/api/v1/provider/store-result",
        headers={"Authorization": f"Basic {analytics_token}"},
        json=results,
    )
    response.raise_for_status()
